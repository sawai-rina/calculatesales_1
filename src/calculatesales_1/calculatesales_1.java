package calculatesales_1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class calculatesales_1 {

	public static void main(String[] args) {

	    //System.out.println("ここにあるファイルを開きます=>" + args[0]);

	//HashMapオブジェクト生成



        Map<String, String> branchMap = new HashMap<>();
        Map<String,Long> salesMap = new HashMap<>();



	    BufferedReader br = null;

		try {
		    //引数１にファイルがある場所
		    //引数２にファイル名を指定
			File file = new File(args[0], "branch.lst");

		    //支店定義ファイルが存在しないとき
			if(!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}


		    //Mapをカンマ区切りで格納
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null) {
				String[] names = line.split(",");

                //配列のサイズが２以外、または店舗番号が数字３桁じゃない場合のエラー
				if( names.length !=2 || !names[0].matches("^[0-9]{3}$")){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
			}
			    branchMap.put(names[0], names[1]);
			    salesMap.put(names[0], 0L);
		}


          //System.out.println(line);

		} catch(IOException e) {

			System.out.println("予期せぬエラーが発生しました");
			return;

		}finally {
			if( br != null) {
				try {
					br.close();
					} catch(IOException e) {
					}
			}
		}


		    //ファイルの抽出
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File file ,String str) {
					if(str.matches("^[0-9]{8}.rcd$")) {
						return true;
					} else {
						return false;
					}
			}
		};


		    //売上ファイルを読み込み、支店コード、売上金額を抽出

		File files = new File(args[0]);
		File[] salesFile= files.listFiles(filter);
		BufferedReader br1 = null;

		for(int i =0; i<salesFile.length; i++) {

			try {


				FileReader fr1 = new FileReader (salesFile[i]);
				br1 = new BufferedReader (fr1);

                //リスト化
				List <String>filesList = new ArrayList<>();

				String lines;
				while((lines = br1.readLine()) !=null) {
					filesList.add(lines);
				}


		        //該当する支店がなかったとき
				if (!branchMap.containsKey(filesList.get(0))) {
					System.out.println( salesFile[i].getName() + "の支店コードが不正です");
					return;
				}

				//売り上げファイルの中身が３行以上ある時のエラーチェック
			    if(filesList.size() >= 3) {
					System.out.println( filesList+"のフォーマットが不正です");
					return;
				 }

		        //売り上げ金額を合算したものをsumに代入
				Long data = Long.parseLong(filesList.get(1));
				Long sum = salesMap.get(filesList.get(0));

				if(sum==null) {
					salesMap.put(filesList.get(0),sum);
				} else {
					sum += data ;
					salesMap.put(filesList.get(0),sum);

					//System.out.println(sum);


		            //合計金額が10桁を超えた時のエラーチェック
				    if(String.valueOf(sum).length()>10) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}
				}
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if ( br1 != null) {
					try {
						br.close();
					} catch(IOException e) {
						return;
					}
			    }
			}
		}
		 //支店別集計ファイル作成

        try {

        	File branchOut = new File(args[0],"branch.out");
		    FileWriter fw = new FileWriter(branchOut);
	        BufferedWriter bw = new BufferedWriter(fw);

            for( String Key :salesMap.keySet()) {
            	bw.write((Key) +",");
           	    bw.write(branchMap.get(Key) + ",");
           	    String line = Long.toString(salesMap.get(Key));
           	    bw.write(line);
           	    bw.newLine();
            }
            bw.close();
        } catch(IOException e) {
        	System.out.println("予期せぬエラーが発生しました");
        	System.out.println(e);
        	return;
        }
    }
}




